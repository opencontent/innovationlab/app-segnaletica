import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, LoadingController, Platform } from '@ionic/angular';
import { Browser } from '@capacitor/browser';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  isSubmitted: boolean;
  type: string = 'password';
  board: any;
  constructor(
    private authService: AuthService,
    private router: Router,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public platform: Platform
  ) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  get errorControl() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.isSubmitted = true;
    if (!this.loginForm.valid) {
      console.log('Please provide all the required values!');
      return false;
    } else {
      this.presentLoading();
    }
  }

  async errorAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Login fallito',
      subHeader: '',
      message: 'Username o password errati. Riprova!',
      buttons: ['Chiudi'],
    });

    await alert.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: '',
      message: 'Richiesta di autenticazione in corso...',
    });
    await loading.present().then(() => {
      this.authService
        .login({
          username: this.loginForm.value.username,
          password: this.loginForm.value.password,
        })
        .subscribe(
          (res) => {
            loading.dismiss();
            this.router.navigateByUrl('home');
          },
          (error) => {
            loading.dismiss();
            this.errorAlert();
          },
          () => {
            loading.dismiss();
          }
        );
    });
  }

  openPage() {
    Browser.open({ url: environment.host + 'user/register' });
  }

  passwordReset() {
    Browser.open({ url: environment.host + '/userpaex/forgotpassword' });
  }

  changeType() {
    if (this.type === 'text') {
      this.type = 'password';
    } else {
      this.type = 'text';
    }
  }
}
