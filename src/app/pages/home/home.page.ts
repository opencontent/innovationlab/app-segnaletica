import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '../../services/storage.service';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

  selectedIndex: any = 0;
  loading: false;
  isLoading = false;
  private unsubscribe$ = new Subject<void>();
  public selectedSection = 'home';
  token: string;
  municipalityData: any;
  counter: number;
  constructor(private router: Router, private storage: StorageService) {}

  ngOnInit() {
    this.storage.getMunicipality().then((res) => {
      if (res) {
        this.municipalityData = JSON.parse(res) || null;
      }
    });

    this.storage.getToken().then((res) => {
      if (res) {
        this.token = JSON.parse(res).token;
      } else {
        this.token = null;
      }
    });
  }

  ionViewWillEnter() {
    this.storage.getMunicipality().then((res) => {
      if (res) {
        this.municipalityData = JSON.parse(res) || null;
      }
    });

    this.storage.getToken().then((res) => {
      if (res) {
        this.token = JSON.parse(res).token;
      } else {
        this.token = null;
      }
    });
  }

  getRoutes(url: string) {
    this.storage.getMunicipality().then((res) => {
      if (res) {
        this.municipalityData = JSON.parse(res) || null;
        if (this.municipalityData) {
          this.router.navigate([url + this.municipalityData.id]);
        } else {
          this.router.navigate(['/municipality-list']);
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  segmentChosen(segment: string) {
    this.selectedSection = segment;
  }

  logout() {
    this.removeToken().then(() => {
      window.location.reload();
    });
  }

  async removeToken() {
    await this.storage.removeToken();
  }

  updateMunicipality(newItem: any) {
    this.municipalityData = newItem;
  }
}
