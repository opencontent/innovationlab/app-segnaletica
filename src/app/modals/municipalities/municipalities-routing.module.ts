import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MunicipalitiesPage } from './municipalities.page';

const routes: Routes = [
  {
    path: '',
    component: MunicipalitiesPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MunicipalitiesPageRoutingModule {}
